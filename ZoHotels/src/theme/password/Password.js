import React, { useState } from 'react';
import {
    Text,
    View,
    SafeAreaView,
    StatusBar,
    Image,
    TextInput,
    TouchableOpacity,
    StyleSheet,
    Alert,
    TouchableWithoutFeedback,
    ScrollView,
    Keyboard
} from 'react-native';

const Password = ({navigation}) => {
    //const [input, setInput] = useState(false);
    const [todos, setTodos] = useState([]);
    const [id, setId] = useState('');
    const [password, setPassword] = useState('');
    const [checkPass, setCheckPass] = useState('');

    clickNext = () => {
        navigation.navigate('ConfirmPass');
    }

    return (
        <SafeAreaView style={styles.container}>
            <StatusBar barStyle="dark-content" />
            <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
                <View style={styles.container}>
                    <View style={styles.header}>
                        <Image source={require('../../asset/images/logo.png')}
                            style={styles.logo} />
                    </View>
                    <View style={styles.content}>
                        <View style={styles.titleContent}>
                            <Text style={styles.title}>Quên mật khẩu</Text>
                        </View>
                        <View style={styles.mainContent}>
                            <ScrollView>
                                <View style={styles.inputBox}>
                                    <Text style={styles.text}>Số điện thoại</Text>
                                    <TextInput
                                        style={styles.input}
                                        placeholder="Nhập số điện thoại"
                                        keyboardType="numeric"
                                        returnKeyType='next'
                                        autoCorrect={false}
                                        onSubmitEditing={() => this.refs.txtPassword.focus()}
                                        onChangeText={(id) => setId(id)}
                                        value={id}
                                    />
                                </View>
                                <View style={styles.inputBox}>
                                    <Text style={styles.text}>Mật khẩu</Text>
                                    <TextInput
                                        style={styles.input}
                                        underlineColorAndroid="transparent"
                                        placeholder="Nhập mật khẩu"
                                        returnKeyType="next"
                                        onSubmitEditing={() => this.refs.txtCheckPass.focus()}
                                        onChangeText={(password) => setPassword(password)}
                                        secureTextEntry
                                        value={password}
                                    />
                                </View>
                                <View style={styles.inputBox}>
                                    <Text style={styles.text}>Nhập lại mật khẩu</Text>
                                    <TextInput
                                        style={styles.input}
                                        underlineColorAndroid="transparent"
                                        placeholder="Nhập lại mật khẩu"
                                        returnKeyType="go"
                                        onChangeText={(checkPass) => setCheckPass(checkPass)}
                                        secureTextEntry
                                        value={checkPass}
                                    />
                                </View>
                                <View style={styles.button}>
                                    <TouchableOpacity
                                        onPress={() => clickNext()}
                                        style={styles.next}>
                                        <Text
                                            style={{
                                                fontFamily: 'Times New Roman',
                                                fontSize: 16,
                                                color: '#fff',
                                                fontWeight: 'bold'
                                            }}
                                        >Tiếp tục</Text>
                                    </TouchableOpacity>
                                </View>
                            </ScrollView>
                        </View>
                    </View>
                </View>
            </TouchableWithoutFeedback>
        </SafeAreaView>
    )
}

export default Password;

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    header: {
        flex: 2,
        justifyContent: 'center',
        alignItems: 'center'
    },
    content: {
        flex: 8,
        marginLeft: 25,
        marginRight: 25
    },
    logo: {
        transform: [{
            scale: 0.7
        }]
    },
    titleContent: {
        flex: 1,
        justifyContent: 'center'
    },
    mainContent: {
        flex: 8,
    },
    footer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    title: {
        fontFamily: 'Times New Roman',
        fontSize: 25,
        fontWeight: 'bold',
    },
    footerText: {
        fontFamily: 'Times New Roman',
        fontSize: 12
    },
    inputBox: {
        marginTop: 10,
        borderBottomColor: '#11111190',
        borderBottomWidth: 1
    },
    input: {
        height: 35,
        fontSize: 14
    },
    text: {
        fontFamily: 'Times New Roman',
        fontSize: 14,
        fontWeight: 'bold'
    },
    next: {
        backgroundColor: '#f99218',
        height: 40,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 4
    },
    button: {
        marginTop: 20
    },
});