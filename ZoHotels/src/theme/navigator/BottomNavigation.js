import * as React from 'react';
import { View, Image, StyleSheet } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';

import HomePage from '../screen/HomePage';
import BookRoom from '../screen/BookRoom';
import Food from '../screen/Food';
import Endow from '../screen/Endow';
import Profile from '../screen/Profile';

const Tab = createMaterialBottomTabNavigator();

function MyTabs() {
    return (
        <Tab.Navigator
            initialRouteName="HomePage"
            barStyle={{ backgroundColor: '#FFFFFF', elevation: 2 }}
        >
            <Tab.Screen
                name="HomePage"
                component={HomePage}
                options={{
                    tabBarLabel: null,
                    tabBarIcon: ({ focused }) => (
                        <Image
                            source={
                                focused
                                    ? require('../../asset/images/trangchu2.png')
                                    : require('../../asset/images/trangchu.png')
                            }
                            //style={styles.logo}
                        />
                    ),
                }}
            />
            <Tab.Screen
                name="BookRoom"
                component={BookRoom}
                options={{
                    tabBarLabel: null,
                    tabBarIcon: ({ focused }) => (
                        <Image
                            source={
                                focused
                                    ? require('../../asset/images/donphong2.png')
                                    : require('../../asset/images/donphong.png')
                            }
                            style={styles.logo}
                        />
                    ),
                }}
            />
            <Tab.Screen
                name="Food"
                component={Food}
                options={{
                    tabBarLabel: null,
                    tabBarIcon: ({ focused }) => (
                        <Image
                            source={
                                focused
                                    ? require('../../asset/images/food2.png')
                                    : require('../../asset/images/food.png')
                            }
                            style={styles.logo}
                        />
                    ),
                }}
            />
            <Tab.Screen
                name="Endow"
                component={Endow}
                options={{
                    tabBarLabel: null,
                    tabBarIcon: ({ focused }) => (
                        <Image
                            source={
                                focused
                                    ? require('../../asset/images/uudai2.png')
                                    : require('../../asset/images/uudai.png')
                            }
                            style={styles.logo}
                        />
                    ),
                }}
            />
            <Tab.Screen
                name="Profile"
                component={Profile}
                options={{
                    tabBarLabel: null,
                    tabBarIcon: ({ focused }) => (
                        <Image
                            source={
                                focused
                                    ? require('../../asset/images/canhan2.png')
                                    : require('../../asset/images/canhan.png')
                            }
                            style={styles.logo}
                        />
                    ),
                }}
            />
        </Tab.Navigator>
    );
}
export default function BottomTabNavigator() {
    return <MyTabs />;
}

const styles = StyleSheet.create({
    logo: {
        transform: [{
            scale: 0.9
        }], 
    },
});
