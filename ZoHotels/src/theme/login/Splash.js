import React, { useState } from 'react';
import {
    View,
    SafeAreaView,
    StatusBar,
    Image,
    StyleSheet
} from 'react-native';

const Splash = () => {
    return (
        <SafeAreaView style={styles.container}>
            <StatusBar barStyle="dark-content" />
            <View style={styles.container}>
                <Image source={require('../../asset/images/splash.png')}
                    style={styles.logo} />
            </View>
        </SafeAreaView>
    )
}

export default Splash;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#F99D1B',
        justifyContent: 'center',
        alignItems: 'center'
    },
    logo: {
        transform: [{
            scale: 1
        }]
    }
});