import React, { useState } from 'react';
import {
    Text,
    View,
    SafeAreaView,
    StatusBar,
    StyleSheet,
    Image,
    TextInput,
    TouchableOpacity,
    Alert,
    ScrollView,
    Keyboard,
    TouchableWithoutFeedback
} from 'react-native';

const Signup = ({ navigation }) => {
    const [todos, setTodos] = useState([]);
    const [name, setName] = useState('');
    const [id, setId] = useState('');
    const [password, setPassword] = useState('');
    const [checkPass, setCheckPass] = useState('');
    const [confirm, setConfirm] = useState('');

    clickSignup = () => {
        Alert.alert(
            'Đăng ký thành công.'
        )
    }
    return (
        <SafeAreaView style={styles.container}>
            <StatusBar barStyle="dark-content" />
            <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
                <View style={styles.container}>
                    <View style={styles.header}>
                        <Image source={require('../../asset/images/logo.png')}
                            style={styles.logo} />
                    </View>
                    <View style={styles.content}>
                        <View style={styles.titleContent}>
                            <Text style={styles.title}>Đăng ký</Text>
                        </View>
                        <View style={styles.mainContent}>
                            <ScrollView>
                                <View style={styles.inputBox}>
                                    <Text style={styles.text}>Họ và tên</Text>
                                    <TextInput style={styles.input}
                                        placeholder="Nhập họ và tên"
                                        returnKeyType='next'
                                        autoCorrect={false}
                                        onChangeText={(name) => setName(name)}
                                        value={name}
                                    />
                                </View>
                                <View style={styles.inputBox}>
                                    <Text style={styles.text}>Số điện thoại</Text>
                                    <TextInput style={styles.input}
                                        placeholder="Nhập số điện thoại"
                                        keyboardType="numeric"
                                        returnKeyType='next'
                                        autoCorrect={false}
                                        onSubmitEditing={() => this.refs.txtPassword.focus()}
                                        onChangeText={(id) => setId(id)}
                                        value={id}
                                    />
                                </View>
                                <View style={styles.inputBox}>
                                    <Text style={styles.text}>Mật khẩu</Text>
                                    <TextInput
                                        style={styles.input}
                                        underlineColorAndroid="transparent"
                                        returnKeyType="next"
                                        onSubmitEditing={() => this.refs.txtCheckPass.focus()}
                                        placeholder="Nhập mật khẩu"
                                        returnKeyType="next"
                                        onChangeText={(password) => setPassword(password)}
                                        secureTextEntry
                                        value={password}
                                    />
                                </View>
                                <View style={styles.inputBox}>
                                    <Text style={styles.text}>Nhập lại mật khẩu</Text>
                                    <TextInput
                                        style={styles.input}
                                        underlineColorAndroid="transparent"
                                        placeholder="Nhập lại mật khẩu"
                                        returnKeyType="next"
                                        onSubmitEditing={() => this.refs.txtConfirm.focus()}
                                        onChangeText={(checkPass) => setCheckPass(checkPass)}
                                        secureTextEntry
                                        value={checkPass}
                                    />
                                </View>
                                <View style={styles.inputBox}>
                                    <Text style={styles.text}>Mã giới thiệu</Text>
                                    <TextInput style={styles.input}
                                        placeholder="Nhập mã giới thiệu"
                                        keyboardType="numeric"
                                        onChangeText={(confirm) => setConfirm(confirm)}
                                        secureTextEntry
                                        value={confirm}
                                    />
                                </View>
                                <View style={{ marginTop: 15 }}>
                                    <TouchableOpacity
                                        onPress={() => clickSignup()}
                                        style={styles.signup}>
                                        <Text
                                            style={{
                                                fontFamily: 'Times New Roman',
                                                fontSize: 16,
                                                color: '#fff',
                                                fontWeight: 'bold'
                                            }}
                                        >Đăng ký</Text>
                                    </TouchableOpacity>
                                </View>
                                <View style={{
                                    marginTop: 15,
                                    flexDirection: 'row',
                                    justifyContent: 'center'
                                }}>
                                    <Text style={{
                                        fontFamily: 'Times New Roman',
                                        fontSize: 14,
                                    }}>Chưa đã tài khoản? </Text>
                                    <TouchableOpacity onPress={() => navigation.navigate('Login')}>
                                        <Text style={{
                                            fontFamily: 'Times New Roman',
                                            fontSize: 14,
                                            color: '#f99218'
                                        }}>Đăng nhập</Text>
                                    </TouchableOpacity>
                                    <Text style={{
                                        fontFamily: 'Times New Roman',
                                        fontSize: 14,
                                    }}> tại đây</Text>
                                </View>
                            </ScrollView>
                        </View>
                    </View>
                </View>
            </TouchableWithoutFeedback>
        </SafeAreaView>
    )
}

export default Signup;

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    header: {
        flex: 2,
        justifyContent: 'center',
        alignItems: 'center'
    },
    content: {
        flex: 8,
        marginLeft: 25,
        marginRight: 25
    },
    logo: {
        transform: [{
            scale: 0.7
        }]
    },
    titleContent: {
        flex: 1,
        justifyContent: 'center'
    },
    title: {
        fontFamily: 'Times New Roman',
        fontSize: 25,
        fontWeight: 'bold',
    },
    mainContent: {
        flex: 9,
        //backgroundColor: 'pink',
    },
    text: {
        fontFamily: 'Times New Roman',
        fontSize: 14,
        fontWeight: 'bold'
    },
    inputBox: {
        marginTop: 10,
        borderBottomColor: '#11111190',
        borderBottomWidth: 1
    },
    input: {
        height: 35,
        fontSize: 14,
    },
    signup: {
        backgroundColor: '#f99218',
        height: 40,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 5
    }
});