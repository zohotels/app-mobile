import React, { useState } from 'react';
import {
    Text,
    View,
    SafeAreaView,
    StatusBar,
    Image,
    TextInput,
    TouchableOpacity,
    StyleSheet,
    Alert,
    TouchableWithoutFeedback,
    ScrollView,
    Keyboard
} from 'react-native';

const Login = ({ navigation }) => {
    const [todos, setTodos] = useState([]);
    const [id, setId] = useState('');
    const [password, setPassword] = useState('');

    clickLogin = () => {
        if (id.length > 0 && password.length > 0) {
            setTodos([...todos, { text: id }])
            setId('')
            setTodos([...todos, { text: password }])
            setPassword('')
            Alert.alert(
                'Đăng nhập thành công. ' +
                'Với: sđt: ' + id + ' và pass: ' + password
            )
        } else {
            navigation.navigate('BottomNavigation');
        }
    }

    return (
        <SafeAreaView style={styles.container}>
            <StatusBar barStyle="dark-content" />
            <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
                <View style={styles.container}>
                    <View style={styles.header}>
                        <Image source={require('../../asset/images/logo.png')}
                            style={styles.logo} />
                    </View>
                    <View style={styles.content}>
                        <View style={styles.titleContent}>
                            <Text style={styles.title}>Đăng nhập</Text>
                        </View>
                        <View style={styles.mainContent}>
                            <ScrollView>
                                <View style={styles.inputBox}>
                                    <Text style={styles.text}>Số điện thoại</Text>
                                    <TextInput style={styles.input}
                                        placeholder="Nhập số điện thoại"
                                        keyboardType="numeric"
                                        returnKeyType='next'
                                        autoCorrect={false}
                                        onSubmitEditing={() => this.refs.txtPassword.focus()}
                                        onChangeText={(id) => setId(id)}
                                        value={id}
                                    />
                                </View>
                                <View style={styles.inputBox}>
                                    <Text style={styles.text}>Mật khẩu</Text>
                                    <TextInput
                                        style={styles.input}
                                        underlineColorAndroid="transparent"
                                        placeholder="Nhập mật khẩu"
                                        returnKeyType="go"
                                        onChangeText={(password) => setPassword(password)}
                                        secureTextEntry
                                        value={password}
                                    />
                                </View>
                                <TouchableOpacity onPress={() => navigation.navigate('Password')}>
                                    <Text style={{
                                        textAlign: 'right',
                                        color: '#f99218',
                                        fontFamily: 'Times New Roman',
                                        fontSize: 12
                                    }}>Quên mật khẩu?</Text>
                                </TouchableOpacity>
                                <View style={styles.button}>
                                    <TouchableOpacity
                                        onPress={() => clickLogin()}
                                        style={styles.login}>
                                        <Text
                                            style={{
                                                fontFamily: 'Times New Roman',
                                                fontSize: 16,
                                                color: '#fff',
                                                fontWeight: 'bold'
                                            }}
                                        >Đăng nhập</Text>
                                    </TouchableOpacity>
                                </View>
                                <View style={{
                                    marginTop: 15,
                                    flexDirection: 'row',
                                    justifyContent: 'center'
                                }}>
                                    <Text style={{
                                        fontFamily: 'Times New Roman',
                                        fontSize: 14,
                                    }}>Chưa có tài khoản? </Text>
                                    <TouchableOpacity onPress={() => navigation.navigate('Signup')}>
                                        <Text style={{
                                            fontFamily: 'Times New Roman',
                                            fontSize: 14,
                                            color: '#f99218'
                                        }}>Đăng ký</Text>
                                    </TouchableOpacity>
                                    <Text style={{
                                        fontFamily: 'Times New Roman',
                                        fontSize: 14,
                                    }}> tại đây</Text>
                                </View>
                            </ScrollView>
                        </View>
                        <View style={styles.footer}>
                            <Text style={styles.footerText}>Điều khoản dịch vụ & Chính sách sử dụng</Text>
                        </View>
                    </View>
                </View>
            </TouchableWithoutFeedback>
        </SafeAreaView>
    )
}

export default Login;

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    header: {
        flex: 2,
        justifyContent: 'center',
        alignItems: 'center'
    },
    content: {
        flex: 8,
        marginLeft: 25,
        marginRight: 25
    },
    logo: {
        transform: [{
            scale: 0.7
        }]
    },
    titleContent: {
        flex: 1,
        justifyContent: 'center'
    },
    mainContent: {
        flex: 8,
    },
    footer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    title: {
        fontFamily: 'Times New Roman',
        fontSize: 25,
        fontWeight: 'bold',
    },
    footerText: {
        fontFamily: 'Times New Roman',
        fontSize: 12
    },
    inputBox: {
        //backgroundColor: 'red',
        marginTop: 10,
        borderBottomColor: '#11111190',
        borderBottomWidth: 1
    },
    input: {
        //backgroundColor: 'blue',
        height: 35,
        fontSize: 14,
        borderBottomColor: '#11111190',
        borderBottomWidth: 1
    },
    text: {
        fontFamily: 'Times New Roman',
        fontSize: 14,
        fontWeight: 'bold'
    },
    login: {
        backgroundColor: '#f99218',
        height: 40,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 4
    },
    button: {
        marginTop: 20
    },
});