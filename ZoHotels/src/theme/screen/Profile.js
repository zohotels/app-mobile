import React, { useState } from 'react';
import {
    Text,
    View,
    SafeAreaView,
    StatusBar,
    StyleSheet,
    ScrollView
} from 'react-native';

const Profile = ({ navigation }) => {
    return (
        <SafeAreaView style={styles.container}>
            <StatusBar barStyle="dark-content" />
            <View style={styles.container}>
                <View style={styles.header}>
                    <Text style={styles.title}>Cá nhân</Text>
                </View>
                <View style={styles.content}>
                    <ScrollView>
                        <View style={{ margin: 30 }}>
                            <Text>a</Text>
                        </View>
                    </ScrollView>
                </View>
            </View>
        </SafeAreaView>
    )
}

export default Profile;

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    header: {
        flex: 1,
        borderBottomColor: '#11111120',
        borderBottomWidth: 1,
        justifyContent: 'flex-end',
        alignItems: 'center'
    },
    content: {
        flex: 9,
    },
    title: {
        fontFamily: 'Times New Roman',
        fontSize: 20,
        fontWeight: 'bold',
    },
});