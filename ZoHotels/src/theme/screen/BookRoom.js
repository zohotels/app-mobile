import React, { useState } from 'react';
import {
    Text,
    View,
    SafeAreaView,
    StatusBar,
    StyleSheet,
    FlatList,
    Image,
    TouchableOpacity
} from 'react-native';

const DATA = [
    {
        id: '1',
        image: require('../../asset/images/bg.png'),
        name: 'ZoHotels Yên Phụ',
        address: '128',
        date: '1111',
        status: 'a'
    },
    {
        id: '2',
        image: require('../../asset/images/bg.png'),
        name: 'ZoHotels Yên Phụ',
        address: '60',
        date: '1111',
        status: 'a'
    },
    {
        id: '3',
        image: require('../../asset/images/bg.png'),
        name: 'ZoHotels Yên Phụ',
        address: '80',
        date: '1111',
        status: 'a'
    },
    {
        id: '4',
        image: require('../../asset/images/bg.png'),
        name: 'ZoHotels Yên Phụ',
        address: '128',
        date: '1111',
        status: 'a'
    },
];

const Item = ({ image, name, address, date, status }) => {
    return (
        <View style={{
            height: 300,
            marginLeft: 20,
            marginRight: 20,
            marginTop: 10,
            borderRadius: 10
        }}>
            <View style={{ flex: 1, }}>
                <Image source={image}
                    style={{
                        borderTopLeftRadius: 10,
                        borderTopRightRadius: 10,
                        width: '100%',
                        height: '100%'
                    }}
                />
            </View>
            <View style={{ flex: 1 }}>
                <Text style={styles.text}>{name}</Text>
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <Image source={require('../../asset/images/vitri.png')}
                        style={{
                            transform: [{
                                scale: 1.5
                            }]
                        }}
                    />
                    <Text style={styles.text2}>{address}</Text>
                </View>
                <Text style={styles.text2}>{date}</Text>
                <Text style={styles.text2}>{status}</Text>
                <View style={{ flexDirection: 'row' }}>
                    <View style={{ flex: 1 }}></View>
                    <View style={{ flex: 1 }}>
                        <TouchableOpacity style={styles.button}
                            onPress={() => navigation.navigate('Login')}>
                            <Text style={{
                                fontFamily: 'Times New Roman',
                                fontSize: 14,
                                color: '#fff',
                                fontWeight: 'bold',
                                paddingLeft: 12,
                                paddingRight: 12
                            }}>Thanh toán</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        </View>
    );
}

const BookRoom = () => {
    const renderItem = ({ item }) => (
        <Item name={item.name}
            image={item.image}
            address={item.address}
            date={item.date}
            status={item.status}
        />
    );
    return (
        <SafeAreaView style={styles.container}>
            <StatusBar barStyle="dark-content" />
            <View style={styles.container}>
                <View style={styles.header}>
                    <Text style={styles.title}>Đơn phòng</Text>
                </View>
                <View style={styles.content}>
                    <FlatList
                        data={DATA}
                        renderItem={renderItem}
                        keyExtractor={item => item.id}
                    />
                </View>
            </View>
        </SafeAreaView>
    )
}

export default BookRoom;

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    header: {
        flex: 1,
        borderBottomColor: '#11111120',
        borderBottomWidth: 1,
        justifyContent: 'flex-end',
        alignItems: 'center'
    },
    content: {
        flex: 9,
    },
    title: {
        fontFamily: 'Times New Roman',
        fontSize: 20,
        fontWeight: 'bold',
    },
    text: {
        fontFamily: 'Times New Roman',
        fontWeight: 'bold',
        fontSize: 16,
        marginTop: 5,
        marginLeft: 10,
        marginRight: 10
    },
    text2: {
        color: '#11111150',
        marginLeft: 5,
        fontFamily: 'Times New Roman',
        fontSize: 12,
        margin: 2
    },
    button: {
        //margin: 20,
        borderRadius: 5,
        backgroundColor: '#F99D1B',
        justifyContent: 'center',
        alignItems: 'center',
        height: 30,
        marginRight: 20,
        marginLeft: 20
    },
});