import React, { useState } from 'react';
import {
    Text,
    View,
    SafeAreaView,
    StatusBar,
    StyleSheet,
    Image,
    ScrollView,
    TouchableHighlight,
    TouchableOpacity,
} from 'react-native';

const HomePage = ({ navigation }) => {
    return (
        <SafeAreaView style={styles.container}>
            <StatusBar barStyle="dark-content" />
            <View style={styles.container}>
                <View style={styles.header}>
                    <Image
                        source={require('../../asset/images/logo2.png')}
                        style={{
                            transform: [{
                                scale: 0.7
                            }]
                        }}
                    />
                </View>
                <View style={styles.content}>
                    <ScrollView>
                        <View style={{
                            marginLeft: 20,
                            marginRight: 20
                        }}>
                            <View style={{ marginTop: 20 }}>
                                <TouchableHighlight
                                    style={styles.input}
                                    onPress={() => navigation.navigate('Login')}
                                >
                                    <View style={{ flexDirection: 'row', alignItems: 'flex-start' }}>
                                        <Image
                                            source={require('../../asset/images/timkiem.png')}
                                            style={{ paddingLeft: 10 }}
                                        />
                                        <Text style={{
                                            fontFamily: 'Times New Roman',
                                            color: '#11111140',
                                            paddingLeft: 5
                                        }}>Tìm kiếm khách sạn</Text>
                                    </View>
                                </TouchableHighlight>
                            </View>
                            <View style={{ marginTop: 20, flexDirection: 'row' }}>
                                <TouchableHighlight style={styles.place}
                                    onPress={() => navigation.navigate('Login')}
                                >
                                    <View style={styles.place}>
                                        <View style={styles.round}>
                                            <Image
                                                source={require('../../asset/images/pin.png')}
                                            />
                                        </View>
                                        <Text style={styles.text}>Gần đây</Text>
                                    </View>
                                </TouchableHighlight>
                                <TouchableHighlight style={styles.place}
                                    onPress={() => navigation.navigate('Login')}
                                >
                                    <View style={styles.place}>
                                        <Image style={styles.round}
                                            source={require('../../asset/images/hanoi.png')}
                                        />
                                        <Text style={styles.text}>Hà Nội</Text>
                                    </View>
                                </TouchableHighlight>
                                <TouchableHighlight style={styles.place}
                                    onPress={() => navigation.navigate('Login')}
                                >
                                    <View style={styles.place}>
                                        <Image style={styles.round}
                                            source={require('../../asset/images/hcm.png')}
                                        />
                                        <Text style={styles.text}>Hồ Chí Minh</Text>
                                    </View>
                                </TouchableHighlight>
                                <TouchableHighlight style={styles.place}
                                    onPress={() => navigation.navigate('Login')}
                                >
                                    <View style={styles.place}>
                                        <Image style={styles.round}
                                            source={require('../../asset/images/danang.png')}
                                        />
                                        <Text style={styles.text}>Đà Nẵng</Text>
                                    </View>
                                </TouchableHighlight>
                            </View>
                            <View style={{ marginTop: 20 }}>
                                <View style={{
                                    flexDirection: 'row',
                                    alignItems: 'flex-end',
                                    marginBottom: 10
                                }}>
                                    <View style={{ flex: 1 }}>
                                        <Text style={styles.text}>Ưu đãi độc quyền</Text>
                                    </View>
                                    <TouchableHighlight
                                        onPress={() => navigation.navigate('Login')}>
                                        <Text style={{
                                            fontSize: 12,
                                            fontFamily: 'Times New Roman',
                                            color: 'red',
                                            fontWeight: 'bold'
                                        }}>Xem tất cả</Text>
                                    </TouchableHighlight>
                                </View>
                                <View style={{
                                    height: 200,
                                    borderRadius: 10
                                }}>
                                    <Image style={{
                                        width: '100%',
                                        height: 200,
                                        borderRadius: 10
                                    }}
                                        source={require('../../asset/images/bg.png')}
                                    />
                                    <View style={{
                                        backgroundColor: '#11111190',
                                        position: 'absolute',
                                        width: '40%',
                                        height: 200,
                                        borderTopLeftRadius: 10,
                                        borderBottomLeftRadius: 10
                                    }}>
                                        <View style={{
                                            flex: 6,
                                            justifyContent: 'center',
                                            alignItems: 'center'
                                        }}>
                                            <Text style={{
                                                color: 'white',
                                                textAlign: 'center'
                                            }}>Mãi bên nhau bạn nhé!!!</Text>
                                        </View>
                                        <View style={{
                                            flex: 4,
                                            justifyContent: 'center',
                                            alignItems: 'center'
                                        }}>
                                            <TouchableOpacity style={styles.button}
                                                onPress={() => navigation.navigate('Login')}>
                                                <Text style={{
                                                    fontFamily: 'Times New Roman',
                                                    fontSize: 14,
                                                    color: '#fff',
                                                    fontWeight: 'bold',
                                                    paddingLeft: 12,
                                                    paddingRight: 12
                                                }}>Đặt ngay!</Text>
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                </View>
                            </View>
                            <View style={{ marginTop: 20 }}>
                                <View style={{
                                    flexDirection: 'row',
                                    alignItems: 'flex-end'
                                }}>
                                    <Text style={styles.text}>Dịch vụ được xác định bởi ZoHotels </Text>
                                    <Text style={[styles.text, styles.color]}> Miễn phí</Text>
                                </View>
                                <View style={{ marginTop: 10, flexDirection: 'row' }}>
                                    <TouchableHighlight style={styles.place}
                                        onPress={() => navigation.navigate('Login')}
                                    >
                                        <View style={styles.place}>
                                            <View style={styles.round2}>
                                                <Image
                                                    source={require('../../asset/images/frontdesk.png')}
                                                />
                                            </View>
                                            <Text style={[styles.text, {
                                                textAlign: 'center',
                                                fontSize: 11,
                                                fontWeight: null
                                            }]}>Front desk 24/24</Text>
                                        </View>
                                    </TouchableHighlight>
                                    <TouchableHighlight style={styles.place}
                                        onPress={() => navigation.navigate('Login')}
                                    >
                                        <View style={styles.place}>
                                            <View style={styles.round2}>
                                                <Image
                                                    source={require('../../asset/images/wifi.png')}
                                                />
                                            </View>
                                            <Text style={[styles.text, {
                                                textAlign: 'center',
                                                fontSize: 11,
                                                fontWeight: null
                                            }]}>Wifi{"\n"}miễn phí</Text>
                                        </View>
                                    </TouchableHighlight>
                                    <TouchableHighlight style={styles.place}
                                        onPress={() => navigation.navigate('Login')}
                                    >
                                        <View style={styles.place}>
                                            <View style={styles.round2}>
                                                <Image
                                                    source={require('../../asset/images/truyenhinh.png')}
                                                />
                                            </View>
                                            <Text style={[styles.text, {
                                                textAlign: 'center',
                                                fontSize: 11,
                                                fontWeight: null
                                            }]}>Truyền hình Cabs 24h</Text>
                                        </View>
                                    </TouchableHighlight>
                                    <TouchableHighlight style={styles.place}
                                        onPress={() => navigation.navigate('Login')}
                                    >
                                        <View style={styles.place}>
                                            <View style={styles.round2}>
                                                <Image
                                                    source={require('../../asset/images/giuong.png')}
                                                />
                                            </View>
                                            <Text style={[styles.text, {
                                                textAlign: 'center',
                                                fontSize: 11,
                                                fontWeight: null
                                            }]}>Giường{"\n"}tinh tươm</Text>
                                        </View>
                                    </TouchableHighlight>
                                    <TouchableHighlight style={styles.place}
                                        onPress={() => navigation.navigate('Login')}
                                    >
                                        <View style={styles.place}>
                                            <View style={styles.round2}>
                                                <Image
                                                    source={require('../../asset/images/bovesinh.png')}
                                                />
                                            </View>
                                            <Text style={[styles.text, {
                                                textAlign: 'center',
                                                fontSize: 11,
                                                fontWeight: null
                                            }]}>Bộ vệ sinh{"\n"}cá nhân</Text>
                                        </View>
                                    </TouchableHighlight>
                                </View>
                            </View>
                            <View style={{ marginTop: 20 }}>
                                <Text style={styles.text}>Khách sạn tốt nhất</Text>
                                <View style={{
                                    height: 220,
                                    marginTop: 10,
                                    flexDirection: 'row',
                                }}>
                                    <View style={styles.option}>
                                        <View style={styles.son}>
                                            <Image style={{
                                                width: '100%',
                                                height: '100%',
                                                borderRadius: 10
                                            }}
                                                source={require('../../asset/images/bg.png')}
                                            />
                                        </View>
                                        <View style={styles.son}>
                                            <TouchableOpacity>
                                                <Text>aaaa</Text>
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                    <View style={styles.option}>
                                        <View style={styles.son}>
                                            <Image style={{
                                                width: '100%',
                                                height: '100%',
                                                borderRadius: 10
                                            }}
                                                source={require('../../asset/images/bg.png')}
                                            />
                                        </View>
                                        <View style={styles.son}>
                                            <TouchableOpacity>
                                                <Text>aaaa</Text>
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                </View>
                            </View>
                        </View>
                    </ScrollView>
                </View>
            </View>
        </SafeAreaView>
    )
}

export default HomePage;

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    header: {
        flex: 1,
        borderBottomColor: '#11111120',
        borderBottomWidth: 1,
        justifyContent: 'flex-end',
        alignItems: 'center'
    },
    content: {
        flex: 9,
    },
    input: {
        borderRadius: 5,
        backgroundColor: 'white',
        elevation: 5,
        margin: 2,
        padding: 10
    },
    round: {
        borderRadius: 999,
        backgroundColor: '#F99D1B',
        width: 60,
        height: 60,
        justifyContent: 'center',
        alignItems: 'center'
    },
    round2: {
        borderRadius: 999,
        backgroundColor: '#F99D1B20',
        width: 40,
        height: 40,
        justifyContent: 'center',
        alignItems: 'center'
    },
    place: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'center',
    },
    text: {
        fontFamily: 'Times New Roman',
        fontWeight: 'bold',
        fontSize: 14,
    },
    button: {
        //margin: 20,
        borderRadius: 20,
        backgroundColor: '#F99D1B',
        justifyContent: 'center',
        alignItems: 'center',
        height: 30,
    },
    color: {
        color: '#11111170',
        fontSize: 12,
    },
    option: {
        flex: 1,
        borderRadius: 20,
        margin: 2
    },
    son: {
        flex: 1
    }
});