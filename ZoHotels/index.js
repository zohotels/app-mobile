/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App';
import Main from './src/theme/navigator/Main';
import Splash from './src/theme/login/Splash';
import {name as appName} from './app.json';
console.disableYellowBox = true;
AppRegistry.registerComponent(appName, () => Main);
